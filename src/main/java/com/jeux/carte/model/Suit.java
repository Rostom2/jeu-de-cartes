package com.jeux.carte.model;

public enum Suit {
    DIAMOND(1), HEART(2), SPADES(3), CLUBS(4);

    int suit;

    private Suit(int value){
        suit = value;
    }
    public int value(){
        return suit;
    }

}
